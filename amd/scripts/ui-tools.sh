#!/bin/bash -eux

export DEBIAN_FRONTEND=noninteractive
sudo su - root


# install openJDK 8 & 11
apt-get update
apt-get install -y -qq openjdk-8-jdk curl jq openjdk-11-jdk

######################################
# install IntelliJ
######################################

mkdir /tmp/int
cd /tmp/int
wget -q -O int.tar.gz "https://download.jetbrains.com/product?code=IIU&latest&distribution=linux" 

#Install to the /opt directory. Should create /opt/ideaIU
mkdir -p /opt/ideaIU
tar -zxf ./int.tar.gz -C /opt/ideaIU --strip-components=1
chown -R vagrant:vagrant /opt/ideaIU

# Set inotify large enough for projects with thousands of files. Required by IntelliJ
echo "fs.inotify.max_user_watches = 524288" > /etc/sysctl.d/idea.conf
sysctl -p --system

echo "[Desktop Entry]
Type=Application
Name=IntelliJ Idea
Comment=IntelliJ
Icon=/opt/ideaIU/bin/idea.svg
Exec=/opt/ideaIU/bin/idea.sh
Terminal=false
Categories=Development;IDE;Java;" > /usr/share/applications/intellij.desktop

#Cleanup the temp directory
cd /tmp
rm -rf /tmp/int

######################################
# install DBeaver
######################################

mkdir /tmp/dbeaver
cd /tmp/dbeaver

wget -q https://dbeaver.io/files/dbeaver-ce_latest_amd64.deb
apt install -qq -y ./dbeaver-ce_latest_amd64.deb

echo "[Desktop Entry]
Comment=
Terminal=False
Name=DBeaver
Icon=/usr/share/dbeaver/dbeaver.png
Exec=dbeaver
Type=Application
Name[en_US]=DBeaver" > /usr/share/applications/dbeaver.desktop

cd /tmp
rm -rf /tmp/dbeaver
