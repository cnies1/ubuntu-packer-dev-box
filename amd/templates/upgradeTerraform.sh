#!/bin/bash -eux

# Move the existing version of terraform over to the terraform11 binary, since terraform 12 has been the standard for a while
if test -f "/usr/local/bin/terraform"; then
	echo  "moving existing terraform binary to terraform11..."
	sudo mv /usr/local/bin/terraform /usr/local/bin/terraform11
	echo  "move successful."
fi

echo "downloading new terraform version"
# Retrieve the newest version of terraform, and set it to be the default terraform binary.
wget -q https://releases.hashicorp.com/terraform/0.12.28/terraform_0.12.28_linux_amd64.zip

echo "extracting new terraform version"
unzip -qq -o terraform_0.12.28_linux_amd64.zip
sudo mv terraform /usr/local/bin/terraform

echo "running terraform --version to confirm"
terraform --version
